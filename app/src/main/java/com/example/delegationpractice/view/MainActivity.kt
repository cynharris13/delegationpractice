package com.example.delegationpractice.view

import androidx.appcompat.app.AppCompatActivity
import com.example.delegationpractice.R

/**
 * Main activity.
 *
 * @constructor Create empty Main activity
 */
class MainActivity : AppCompatActivity(R.layout.activity_main)
