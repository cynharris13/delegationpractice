package com.example.delegationpractice.view.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.delegationpractice.R
import com.example.delegationpractice.databinding.FragmentThirdBinding
import com.example.delegationpractice.delegates.ToolbarDelegate
import com.example.delegationpractice.delegates.ToolbarDelegateImpl

/**
 * A simple [Fragment] subclass as the default destination in the navigation.
 */
class ThirdFragment : Fragment(), ToolbarDelegate by ToolbarDelegateImpl() {

    private var _binding: FragmentThirdBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        setActivityForToolbarDelegate(requireActivity() as AppCompatActivity)
        addToolbar(
            toolbar = binding.toolbar,
            title = "red toolbar",
            enableHomeAsUp = true,
            bg = R.color.red
        )
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.buttonFirst.setOnClickListener {
            findNavController().navigate(R.id.action_thirdFragment_to_FirstFragment)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
