package com.example.delegationpractice.delegates

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.content.res.AppCompatResources
import com.example.delegationpractice.R
import com.google.android.material.appbar.MaterialToolbar

/**
 * Toolbar delegate implementation.
 *
 * @constructor Create empty Toolbar delegate impl
 */
class ToolbarDelegateImpl : ToolbarDelegate {
    private var toolbar: MaterialToolbar? = null
    private var activity: AppCompatActivity? = null

    override fun setActivityForToolbarDelegate(activity: AppCompatActivity?) {
        this.activity = activity
    }

    override fun addToolbar(
        toolbar: MaterialToolbar,
        title: String?,
        enableHomeAsUp: Boolean,
        homeAsUpDrawable: Drawable?,
        bg: Int?
    ) {
        toolbar.background = AppCompatResources.getDrawable(activity!!.applicationContext, bg ?: R.color.white)
        this.toolbar = toolbar
        setToolbarTitle(title)

        activity?.setSupportActionBar(toolbar)

        activity?.supportActionBar?.let { supportActionBar ->
            supportActionBar.setDisplayHomeAsUpEnabled(enableHomeAsUp)
            homeAsUpDrawable?.let {
                supportActionBar.setHomeAsUpIndicator(it)
            }
        }
    }

    override fun setToolbarTitle(newTitle: String?) {
        toolbar?.title = newTitle
    }
}
