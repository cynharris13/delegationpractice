package com.example.delegationpractice.delegates

import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.appbar.MaterialToolbar

/**
 * Toolbar delegate interface.
 *
 * @constructor Create empty Toolbar delegate
 */
interface ToolbarDelegate {
    /**
     * Add toolbar.
     *
     * @param toolbar
     * @param title
     * @param enableHomeAsUp
     * @param homeAsUpDrawable
     * @param bg
     */
    fun addToolbar(
        toolbar: MaterialToolbar,
        title: String? = null,
        enableHomeAsUp: Boolean = false,
        homeAsUpDrawable: Drawable? = null,
        bg: Int? = null
    )

    /**
     * Set toolbar title.
     *
     * @param newTitle
     */
    fun setToolbarTitle(newTitle: String?)

    /**
     * Set activity for toolbar delegate.
     *
     * @param activity
     */
    fun setActivityForToolbarDelegate(activity: AppCompatActivity?)
}
